<?php
/*Autor: David Regueiro Gomez
Fecha:08/05/2019
Version: 1.0-->
vamos a hacer el controlador para nuestro modulo de Drupal
tendra varios metodos que usaremos para hacer operaciones crud en las tablas*/
namespace Drupal\elecciones\Controller;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Database;
use Drupal\Core\Url;

class resultradoController extends ControllerBase {
  public function leerresultado() {
    //metemos en un array las opciones tque tendra la cabecera de nuestra tabla
    $header_table = array(
      'idpartido'=> t('Identificador'),
      'nombre' => t('Nombre Partido'),
      'Diputados' => t('Numero de diputados'),
      'opt' => t('Operación 1'),
      'opt1' => t('Operación 2'),
    );
    // hacemos el select sobre nuestra tabla de elecciones
    $query = \Drupal::database()->select('resultado', 'res');
    $query->fields('res',
    ['idpartido','nombre','Diputados']);
    $query-> orderBy('res.Diputados','DESC');
    $resultados = $query->execute()->fetchAll();
    $rows=array();
    // vamos metiendo cada fila en el array data
    foreach($resultados as $data){
      // y tambien hacemos un enlace en cada fila para editar o eliminarla.
      $delete = Url::fromUserInput('/elecciones/form/borrarresultado/'.$data->idpartido);
      $edit = Url::fromUserInput('/elecciones/partidos?num='.$data->idpartido);
      // Guarda cada fila en el array rows que visualizaremos mas adelante.
      $rows[] = array(
        'idpartido' =>$data->idpartido,
        'nombre' => $data->nombre,
        'Diputados' => $data->Diputados,
        // Agregamos los enlaces de borrar y editar
        \Drupal::l('Eliminar', $delete),
        \Drupal::l('Editar', $edit),
      );
    }
    // Aqui realizamos las operaciones para crear nuestra grafica,declaramos array,recorremos los datos y creamos el script.
    $dataPoints = array();
    foreach($resultados as $row){
      array_push($dataPoints, array("y"=> $row->Diputados, "label" => "$row->nombre"));
    }
    $link = null;
    ?>
    <script>
    window.onload = function() {
      var chart = new CanvasJS.Chart("chartContainer", {
        animationEnabled: true,
        title: {
          text: "Resultados Elecciones Generales"
        },
        subtitles: [{
          text: "Abril 2019"
        }],
        data: [{
          type: "pie",
          yValueFormatString: "### 'Escaños'",
          indexLabel: "{label} ({y})",
          dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
        }]
      });
      chart.render();
    }
    </script>
    <?php
    // generamos la tabla donde incluiremos la cabecera y los datos.
    $datosf['table'] = [
      '#type' => 'table',
      '#header' => $header_table,
      '#rows' => $rows,
      '#empty' => t('Non se atoparon Partidos'),];


      //hacemos la consulta para sumar el numero total de votos
      $query = \Drupal::database()->query('select sum(Diputados) suma from resultado');
      $resultados2 = $query->fetch();
 
      //lo devolvemos en un array para sacarlo en una vista de TWYG
      return array(
        '#theme' => 'resultado',
        '#titulo' => $this->t('Partidos Politicos'),
        '#descricion' => $this->t('Resultados elecciones'),
        '#datosf' => $datosf,
        '#suma' =>  $resultados2->suma
      );
    }
    // Sacamos el formulario de partidos para poder sacarlo en una vista TWYG
    public function verForm() {
      $verForm = $this->formBuilder()->getForm('\Drupal\elecciones\Form\partidos');

      return array(
        '#theme' => 'verForm',
        '#titulo' => $this->t('Partidos Politicos'),
        '#descricion' => $this->t('Resultados elecciones'),
        '#verForm' => $verForm
      );
    }
    // Sacamos el formulario de votar para poder sacarlo en una vista TWYG

    public function votar() {
      $votar = $this->formBuilder()->getForm('\Drupal\elecciones\Form\votacion');
      return array(
        '#theme' => 'votar',
        '#titulo' => $this->t('Partidos Politicos'),
        '#descricion' => $this->t('Resultados elecciones'),
        '#votar' => $votar,
      );
    }
  }
  ?>
