<?php

namespace Drupal\elecciones\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormInterface;



/**
 * Provides a 'formulario' block.
 *
 * @Block(
 *  id = "formulario",
 *  admin_label = @Translation("Formulario"),
 * )
 */
class formulario extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $votar = \Drupal::formBuilder()->getForm('\Drupal\elecciones\Form\votacion');
    return $votar;
  }

}
