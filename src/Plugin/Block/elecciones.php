<?php

namespace Drupal\elecciones\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'elecciones' block.
 *
 * @Block(
 *  id = "elecciones",
 *  admin_label = @Translation("Elecciones"),
 * )
 */
class elecciones extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {


    $html = "<p><a href=/drupal/elecciones/leerresultado>1. Ver Resultado</a></p>".
    "<p><a href=/drupal/elecciones/votar>2. Votar</a></p>".
    "<p><a href=/drupal/elecciones/partidos>3. Añadir Partido</a></p>";
    $build = [];
    $build['elecciones']['#markup'] = $html;

    return $build;
  }

}
