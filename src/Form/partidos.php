<?php
/*Autor: David Regueiro Gomez
Fecha:08/05/2019
Version: 1.0-->
a este formulario llegaremos de forma directa para insertar un registro nuevo, o editando uno ya existente desde la tabla de registros*/
namespace Drupal\elecciones\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Symfony\Component\HttpFoundation\RedirectResponse;

class partidos extends FormBase {

  public function getFormId() {
    return 'partidos';
  }
  //Definimos la estructura del formulariuo
  public function buildForm(array $form, FormStateInterface $form_state) {
    $conexion = Database::getConnection();
    $rexistro = array();
    // si le llega un id recupera los datos de ese partido
    if (isset($_GET['num'])) {
      $query = $conexion->select('resultado', 'res')
      ->condition('idpartido', $_GET['num'])
      ->fields('res');
      $rexistro = $query->execute()->fetchAssoc();
    }
    //Definimos el campo para el nombre del partido, el numero de diputados y el submit.
    $form['candidato_nombre'] = array(
      '#type' => 'textfield',
      '#title' => t('nombre:'),
      '#required' => TRUE,
      '#default_value' => (isset($rexistro['nombre']) && $_GET['num']) ?
      $rexistro['nombre']:'',
    );
    $form['candidato_Diputados'] = array(
      '#type' => 'textfield',
      '#title' => t('Diputados:'),
      '#required' => TRUE,
      '#default_value' => (isset($rexistro['Diputados']) && $_GET['num']) ?
      $rexistro['Diputados']:'',
    );
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    return $form;
  }
  //Validamos los dos campos, el nombre solo letras y diputados solo numeros
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $nome = $form_state->getValue('candidato_nombre');
    $diputados = $form_state->getValue('candidato_Diputados');
    if(preg_match('/[^A-Z a-z]/', $nome)) {
      $form_state->setErrorByName('candidato_nombre', $this->t('O nome só pode
      conter caracteres.'));
    }
    if(preg_match('/[^0-9]/', $diputados)) {
      $form_state->setErrorByName('candidato_Diputados', $this->t('Solo pueden ser numeros'));
    parent::validateForm($form, $form_state);
  }
}
  // cuando enviamos el formulario se obtienen los valores y se asignan a variables
  // si $_GET['num'] tiene valor hacemos update y si no haremos un insert.
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $field = $form_state->getValues();
    $nome = $field['candidato_nombre'];
    $Diputados = $field['candidato_Diputados'];
    if (isset($_GET['num'])) {
      // UPDATE
      $field = array(
        'nombre' => $nome,
        'Diputados' => $Diputados,
      );
      $query = \Drupal::database();
      $query->update('resultado')
      ->fields($field)
      ->condition('idpartido', $_GET['num'])
      ->execute();
      drupal_set_message("Actualizado correctamente.");
      $form_state->setRedirect('elecciones.resultrado_controller_leerresultado');

    }
    else
    {
      // INSERT
      $field = array(
        'nombre' => $nome,
        'Diputados' => $Diputados,
      );
      $query = \Drupal::database();
      $query ->insert('resultado')
      ->fields($field)
      ->execute();
      drupal_set_message("Inserido correctamente.");
      $response = new
      RedirectResponse(\Drupal::url('elecciones.resultrado_controller_leerresultado'));
      $response->send();
    }
  }

}
