<?php
/*Autor: David Regueiro Gomez
Fecha:08/05/2019
Version: 1.0-->
Este formulario lo usaremos para votar en nuestras eleccionas, cada vez que alguien vote
 a un partido a este se le aumentara un voto.*/
namespace Drupal\elecciones\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Symfony\Component\HttpFoundation\RedirectResponse;

class votacion extends FormBase {
  public function getFormId() {
    return 'votacion';
  }
//lo primero que haremos sera seleccionar todos los nombres de lo partidos de nuestra base de datos
  public function buildForm(array $form, FormStateInterface $form_state) {
    $query = \Drupal::database()->select('resultado', 'res');
    $query->fields('res',
    ['nombre']);
    $resultados = $query->execute()->fetchAll();
    $rows=array();
    // en el array rows, metemos por un lado el campo y el valor, que en este caso son todos los nombres de los partidos ambos
    foreach($resultados as $data){
      $rows[] = array(
        $data->nombre=>$data->nombre
      );
    }
//definimos la estructura de nuestro formulario que permitira votar un partido en un menu desplegable
    $form['votar_partido'] = array (
      '#type' => 'select',
      '#title' => t('Partido'),
      '#options' => $rows,
    );
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit')
    ];
    return $form;
  }
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }
// una vez llegados aqui hacemos el update sobre la tabla de los partidos, usando la operacion expression.
  public function submitForm(array &$form, FormStateInterface $form_state) {
   $field = $form_state->getValues();
    $nome = $field['votar_partido'];
    $query = \Drupal::database();
    $query->update('resultado')
    ->condition('nombre', $nome)
    ->expression('Diputados', 'Diputados + :n', [':n' =>1])
    ->execute();
    drupal_set_message('Tu voto ha sido registrado');
    $form_state->setRedirect('elecciones.resultrado_controller_leerresultado');
  }
}
?>
