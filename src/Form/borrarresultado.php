<?php
/*Autor: David Regueiro Gomez
Fecha:08/05/2019
Version: 1.0-->
vamos a hacer un formulario que nos permita borrar filas pidiendonos confirmacion*/
namespace Drupal\elecciones\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Url;
use Drupal\Core\Render\Element;

class borrarresultado extends ConfirmFormBase {
  public $cid;
  private $id;
  public function getFormId() {
    return 'delete_form';
  }
  //pedimos la confirmacion de que se desea eliminar el registro
  public function getQuestion() {
    return t('¿Seguro que quieres eliminar el partido con id: %cid?',
    array('%cid' => $this->id));
  }
  public function getCancelUrl() {
    return new Url('elecciones.resultrado_controller_leerresultado');
  }
  public function getDescription() {
    return t('Solo deberias eliminarlo si estas seguro');
  }
  public function getConfirmText() {
    return t('Eliminar');
  }
  public function getCancelText() {
    return t('Cancelar');
  }
  public function buildForm(array $form, FormStateInterface $form_state, $cid
  = NULL) {
    $this->id = $cid;
    return parent::buildForm($form, $form_state);
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }
  //Ejecutamos la funcion de borrar la fila identificada por el ID.
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $query = \Drupal::database();
    $query->delete('resultado')
    ->condition('idpartido',$this->id)
    ->execute();
    drupal_set_message("Eliminado correctamente.");
    $form_state->setRedirect('elecciones.resultrado_controller_leerresultado');
  }
}
?>
